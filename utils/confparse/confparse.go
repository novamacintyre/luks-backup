package confparse

import (
	"bufio"
	"fmt"
	"path/filepath"
	"sort"
	"strings"

	"github.com/google/uuid"
)

type Device struct {
	uuid uuid.UUID
}

type Node struct {
	Dev  Device
	Path string
}

type Edge struct {
	SrcNode Node
	DstNode Node
}

type Conf struct {
	Devices []Device
	Aliases map[Device][]Device
	Nodes   []Node
	Edges   []Edge
}

func (c *Conf) AddDevice(devString string) {
	// Make sure you can pass in an ACTUAL DEVICE
	newDev, _ := NewDevice(devString)
	c.Devices = append(c.Devices, newDev)
}

func (c *Conf) RemoveDevice(devString string) error {
	ok := false
	for idx, dev := range c.Devices {
		ds := dev.String()
		if ds == devString {
			if idx == len(c.Devices)-1 {
				c.Devices = c.Devices[:idx]
			} else {
				c.Devices = append(c.Devices[:idx], c.Devices[idx+1:]...)
			}
			ok = true
			break
		}
	}
	if !ok {
		return fmt.Errorf("Could not remove Device: %v", devString)
	}
	return nil
}

func NewDevice(devString string) (Device, error) {
	parsedUUID, err := uuid.Parse(devString)
	if err != nil {
		return Device{}, fmt.Errorf("could not parse Device uuid: %v", err)
	}
	return Device{uuid: parsedUUID}, nil
}

func NewNode(nodeString string) (Node, error) {
	devId, path, found := strings.Cut(nodeString, ":")
	if !found {
		return Node{}, fmt.Errorf("could not parse Node: %v", nodeString)
	}
	dev, err := NewDevice(devId)
	if err != nil {
		return Node{}, err
	}
	path = filepath.Clean(path)
	return Node{Dev: dev, Path: path}, nil
}

func NewEdge(edgeString string) (Edge, error) {
	if strings.Count(edgeString, "->") != 1 {
		return Edge{}, fmt.Errorf("invalid edgeString '%s'", edgeString)
	}
	srcNodeString, dstNodeString, found := strings.Cut(edgeString, "->")
	if !found {
		return Edge{}, fmt.Errorf("could not parse Edge: %v", edgeString)
	}
	srcNode, err := NewNode(srcNodeString)
	if err != nil {
		return Edge{}, err
	}
	dstNode, err := NewNode(dstNodeString)
	if err != nil {
		return Edge{}, err
	}
	return Edge{SrcNode: srcNode, DstNode: dstNode}, nil
}

func (dev Device) String() string {
	return dev.uuid.String()
}

func (n Node) String() string {
	return fmt.Sprintf("%s:%s", n.Dev.String(), n.Path)
}

func (e Edge) String() string {
	return fmt.Sprintf("%s->%s", e.SrcNode, e.DstNode)
}

func isNodeSubset(superV Node, subV Node) bool {
	// OK, strings.Contains does not do what you expect here
	// What if you have /dir1 and /dir2/dir1? Or /dir1 and /dir1111?
	superPathParts := strings.Split(superV.Path, "/")
	subPathParts := strings.Split(subV.Path, "/")
	if superV.Dev == subV.Dev {
		for i, p := range superPathParts {
			if subPathParts[i] != p {
				return false
			}
		}
		return true
	}
	return false
}

func getDstNodes(edges []Edge) []Node {
	var nodes []Node
	for _, e := range edges {
		nodes = append(nodes, e.DstNode)
	}
	return nodes
}

// isNodeInjective returns true <==> Node is not a subset of another DstNode
func isNodeInjective(n Node, dstNodes []Node) bool {
	var isMappedTo bool = false
	for _, n0 := range dstNodes {
		if isNodeSubset(n, n0) {
			if isMappedTo {
				return false
			}
			isMappedTo = true
		}
	}
	return true
}

// isEdgeValid returns false <==> e Node devices are the same
func isEdgeValid(e Edge) error {
	if e.SrcNode.Dev == e.DstNode.Dev {
		return fmt.Errorf("invalid Edge: src dev and dst dev are the same (%#v)", e)
	}
	return nil
}

func DiffDevices(origDeviceList []Device, newDeviceList []Device) (removed []Device, added []Device, common []Device) {
	// We'll take out devices as they're found in orig. What's left is net new
	// Attn: Nova: Something interesting about this little thing: all it's doing is copying newDeviceList
	//   to added. Why can't we just do added = newDeviceList? Well, when we do, newDeviceList will mutate
	//   under any transformations performed on added. Not sure why this is, and a point of confusion I
	//   ought to clear up!
	sort.Slice(origDeviceList, func(i, j int) bool { return origDeviceList[i].String() < origDeviceList[j].String() })
	sort.Slice(newDeviceList, func(i, j int) bool { return newDeviceList[i].String() < newDeviceList[j].String() })
	added = append([]Device(nil), newDeviceList...)
	// TODO: Sort Device list, etc. Then we can speed this up
	for _, od := range origDeviceList {
		inNewDeviceList := false
		for i, nd := range newDeviceList {
			if od == nd {
				inNewDeviceList = true
				// idxOffset accounts for added getting shorter each time a match is found
				idxOffset := len(newDeviceList) - len(added)
				added = append(added[:i-idxOffset], added[i+1-idxOffset:]...)
				common = append(common, nd)
				break
			}
		}
		if !inNewDeviceList {
			removed = append(removed, od)
		}
	}
	return
}

func DiffConf(origConf Conf, newConf Conf) {
}

// ValidateConf returns a list of invalid nodes in passed Conf.
// In addition, returns check value true <==> the following are true:
// 1. All nodes point to a declared Device
// 2. No two SrcNodes map to the same DstNode
// 3. All edges are valid
func ValidateConf(conf Conf) (map[Node]error, map[Edge]error, bool) {
	dstNodes := getDstNodes(conf.Edges)
	invalidNodes := map[Node]error{}
	invalidEdges := map[Edge]error{}
	for _, n := range conf.Nodes {
		dev := n.Dev
		ok := false
		for _, d := range conf.Devices {
			if dev == d {
				ok = true
			}
		}
		if !ok {
			invalidNodes[n] = fmt.Errorf("invalid Node: Node Device not declared")
		}
	}
	for _, n := range dstNodes {
		if !isNodeInjective(n, dstNodes) {
			invalidNodes[n] = fmt.Errorf("mapping error: Node is mapped to multiple times")
		}
	}
	for _, e := range conf.Edges {
		if err := isEdgeValid(e); err != nil {
			invalidEdges[e] = err
		}
	}
	confIsValid := (len(invalidNodes) == 0) && (len(invalidEdges) == 0)
	return invalidNodes, invalidEdges, confIsValid
}

func ParseConf(data []byte) (Conf, map[Node]error, map[Edge]error, bool) {
	scanner := bufio.NewScanner(
		strings.NewReader(string(data)),
	)
	var conf Conf
	var invalidNodes map[Node]error
	var invalidEdges map[Edge]error
	var ok = true
	var section string
	var line string
	for scanner.Scan() {
		line = string(scanner.Text())
		if len(line) < 2 {
			continue
		}
		if line[0] == '[' && line[len(line)-1] == ']' {
			section = line[1 : len(line)-1]
			continue
		}
		switch section {
		case "DEVICES":
			parsedUUID, _ := uuid.Parse(line)
			d := Device{uuid: parsedUUID}
			conf.Devices = append(conf.Devices, d)
		case "NODES":
			parsedNode, _ := NewNode(line)
			conf.Nodes = append(conf.Nodes, parsedNode)
		case "EDGES":
			parsedEdge, _ := NewEdge(line)
			conf.Edges = append(conf.Edges, parsedEdge)
		}
	}
	invalidNodes, invalidEdges, ok = ValidateConf(conf)
	return conf, invalidNodes, invalidEdges, ok
}
