module luksbackup

go 1.19

require (
	github.com/gdamore/tcell/v2 v2.5.3
	github.com/godbus/dbus/v5 v5.1.0
	github.com/google/uuid v1.3.0
	github.com/rivo/tview v0.0.0-20221221172820-02e38ea9604c
)

require (
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/rivo/uniseg v0.4.2 // indirect
	golang.org/x/sys v0.0.0-20220318055525-2edf467146b5 // indirect
	golang.org/x/term v0.0.0-20210220032956-6a3ed077a48d // indirect
	golang.org/x/text v0.3.7 // indirect
)
